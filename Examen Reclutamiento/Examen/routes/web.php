<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\SeguimientoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

/*Route::get('/clientes', function () {
    return "aqui se devolvera clientes";
});*/

Route::resource('/clientes',ClienteController::class);
Route::resource('/proveedor',ProveedorController::class);
Route::resource('/seguimiento',SeguimientoController::class);

Route::get('sendemail',function(){

    $data= array(
        'name'=>"Se registro un nuevo envio"
    );
    Mail::send('mail.email',$data, function($message){
        $message->from('019030237@upq.edu.mx','Se registro un nuevo envio');

        $message->to('019030237@upq.edu.mx')->subject('test email');
    });
    return"Se envio un correo correctamente";
});