<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Guía de seguimiento</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('img/favicon.png')}}" rel="icon">
  <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

</head>

<body>
    <div class="row">    
        <div id="contact" class="box-shadow-full">
            <div class="row">
                <a href="/"><button type="button" class="btn btn-primary btn-lg">Inicio</button></a>
                <h4>Gestión de base de datos</h4>
                <div class="row">
                <div class="col-xl-12">
                        <form action="{{route('proveedor.index')}}" method="get">
                                <div class="form-row">
                                    <div class="col-sm-4 my-1">
                                        <input type="text" class="form-control" name="texto">
                                    </div>
                                    <div class="col-auto mt-1">
                                        <input type="submit" class="btn btn-primary" value="Buscar">
                                    </div>
                                    <div class="col-auto mt-1">
                                        <a href="{{route('proveedor.create')}}" class="btn btn-success">Nuevo</a>
                                    </div>
                                </div>
                        </form> 
                        </div>
                    <div class="col-lx-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Opciones</th>
                                        <th>Id proveedor</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Numero Celular</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($proveedor)<=0)
                                        <tr>
                                            <td colspan="5">No hay resultados</td>
                                        </tr>
                                    @else
                                    @foreach ($proveedor as $proveedores)
                                        <tr>
                                        <td><a href="{{route('proveedor.edit',$proveedor->id_proveedor)}}" class="btn btn-warning btn-sm">Editar </a> 
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{$proveedor->id_proveedor}}">
                                                Eliminar
                                            </button></td>
                                        <td>{{$proveedores->id_proveedor}}</td>
                                        <td>{{$proveedores->nombre}}</td>
                                        <td>{{$proveedores->apellido}}</td>
                                        <td>{{$proveedores->numero_celular}}</td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                
              