<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Guía de seguimiento</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('img/favicon.png')}}" rel="icon">
  <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

</head>

<body>
    <div class="row">    
        <div id="contact" class="box-shadow-full">
            <div class="row">
                <a href="/"><button type="button" class="btn btn-primary btn-lg">Inicio</button></a>
                <div class="container">
                    <h4>Gestión de base de datos</h4>
                    <div class="row">
                        <div class="col-xl-12">
                        <form action="{{route('clientes.index')}}" method="get">
                                <div class="form-row">
                                    <div class="col-sm-4 my-1">
                                        <input type="text" class="form-control" name="texto">
                                    </div>
                                    <div class="col-auto mt-1">
                                        <input type="submit" class="btn btn-primary" value="Buscar">
                                    </div>
                                    <div class="col-auto mt-1">
                                        <a href="{{route('clientes.create')}}" class="btn btn-success">Nuevo</a>
                                    </div>
                                </div>
                        </form> 
                        </div>
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Opciones</th>
                                            <th>Id Pedido</th>
                                            <th>Nombre</th>
                                            <th>Localización</th>
                                            <th>Tiempo en horas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($clientes)<=0)
                                            <tr>
                                                <td colspan="5">No hay resultados</td>
                                            </tr>
                                        @else
                                        @foreach ($clientes as $cliente)
                                            <tr>
                                            <td><a href="{{route('clientes.edit',$cliente->id_clientes)}}" class="btn btn-warning btn-sm">Editar </a> 

                                            
                                            </td>
                                            <td>{{$cliente->id_clientes}}</td>
                                            <td>{{$cliente->nombre}}</td>
                                            <td>{{$cliente->apellido}}</td>
                                            <td>{{$cliente->numero_celular}}</td>
                                            </tr>
                                            @include('bd.delete')
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                {{$clientes->links()}}
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>                
</html>