<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Guía de seguimiento</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('img/favicon.png')}}" rel="icon">
  <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

</head>

<body>
    <div class="row">    
        <div id="contact" class="box-shadow-full">
            <div class="row">
                <a href="/"><button type="button" class="btn btn-primary btn-lg">Inicio</button></a>
                <h4>Gestión de base de datos</h4>
                <div class="row">
                    <div class="col-lx-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Opciones</th>
                                        <th>Id seguimiento</th>
                                        <th>localizacion</th>
                                        <th>Tiempo</th>
                                        <th>Transportista</th>
                                        <th>cliente</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($seguimiento as $seguimientos)
                                        <tr>
                                        <td>Editar | Eliminar</td>
                                        <td>{{$seguimientos->id_seguimiento}}</td>
                                        <td>{{$seguimientos->localizacion}}</td>
                                        <td>{{$seguimientos->Tiempo}}</td>
                                        <td>{{$seguimientos->Transportista}}</td>
                                        <td>{{$seguimientos->cliente}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                
              