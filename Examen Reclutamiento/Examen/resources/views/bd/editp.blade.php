<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear registro</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <h4>Actualizar proveedor</h4> 
        <div class="row">
            <div class="col-xl-12">
                <form action="{{route('proveedor.update',$proveedor->id_proveedor)}}" method="post">
                    @csrf
                    @method ('PUT')
                    <div class="form-group">
                        <label for="id_proveedor">Id proveedor(1,2,3,....)</label>
                        <input type="text" class="form-control" name="id_proveedor" required maxlength="11" value="{{$proveedor->id_proveedor}}">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" required maxlength="50" value="{{$proveedor->nombre}}">
                    </div>
                    <div class="form-group">
                        <label for="apellido">Apellidos</label>
                        <input type="text" class="form-control" name="apellido" required maxlength="50" value="{{$proveedor->apellido}}">
                    </div>
                    <div class="form-group">
                        <label for="numero_celular">Numero Celular</label>
                        <input type="text" class="form-control" name="numero_celular" required maxlength="10" value="{{$proveddor->numero_celular}}">
                    </div>
                    <div class="class">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                        <input type="reset" class="btn btn-default" value="Cancelar">
                        <a href="javascript:history.back()">Ir al listado</a>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</body>
</html>