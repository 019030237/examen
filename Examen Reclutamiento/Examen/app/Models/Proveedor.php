<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    // HasFactory;
    protected $table="proveedores";
    protected $primaryKey="id_proveedor";
    protected $fillable=[
        'nombre','apellido','numero_celular'
    ];

    public $timestamps=false;
}
