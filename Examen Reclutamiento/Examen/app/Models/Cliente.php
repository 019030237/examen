<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    // HasFactory;
    protected $table="clientes";
    protected $primaryKey="id_clientes";
    protected $fillable=[
        'nombre','apellido','numero_celular'
    ];

    public $timestamps=false;
}
