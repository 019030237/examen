<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model
{
    // HasFactory;
    protected $table="seguimiento";
    protected $primaryKey="id_seguimiento";
    protected $fillable=[
        'localizacion','tiempo','transportista','cliente'
    ];
}
