<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $text=trim($request->get('texto'));
        $clientes=DB::table('clientes')
                        ->select('id_clientes','nombre','apellido','numero_celular')
                        ->where('apellido','LIKE','%'.$text.'%')
                        ->orWhere('nombre','LIKE','%'.$text.'%')
                        ->orderBy('apellido','asc')
                        ->paginate(10);
        return view('bd.clientes',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bd.createc');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente=new CLiente;
        $cliente->id_clientes=$request->input('id_clientes');
        $cliente->nombre=$request->input('nombre');
        $cliente->apellido=$request->input('apellido');
        $cliente->numero_celular=$request->input('numero_celular');
        $cliente->save();
        return redirect()->route('clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_clientes)
    {
        $cliente=Cliente::findOrFail($id_clientes);
        return view('bd.editc',compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_clientes)
    {
        $cliente=Cliente::findOrFail($id_clientes);
        $cliente->id_clientes=$request->input('id_clientes');
        $cliente->nombre=$request->input('nombre');
        $cliente->apellido=$request->input('apellido');
        $cliente->numero_celular=$request->input('numero_celular');
        $cliente->save();
        return redirect()->route('clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_clientes)
    {
        $cliente=Cliente::findOrFail($id_clientes);
        $cliente->delete();
        return redirect()->route('clientes.index');
    }
}
