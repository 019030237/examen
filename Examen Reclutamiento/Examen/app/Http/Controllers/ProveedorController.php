<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proveedor;
use Illuminate\Support\Facades\DB;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $text=trim($request->get('texto'));
        $proveedor=DB::table('proveedores')
                        ->select('id_proveedor','nombre','apellido','numero_celular')
                        ->where('apellido','LIKE','%'.$text.'%')
                        ->orWhere('nombre','LIKE','%'.$text.'%')
                        ->orderBy('apellido','asc')
                        ->paginate(10);
        return view('bd.proveedor',compact('proveedor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bd.cretatep');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proveedor=new Proveedor;
        $proveedor->id_proveedor=$request->input('id_proveedor');
        $proveedor->nombre=$request->input('nombre');
        $proveedor->apellido=$request->input('apellido');
        $proveedor->numero_celular=$request->input('numero_celular');
        $proveedor->save();
        return redirect()->route('proveedor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_proveedor)
    {
        $proveedor=Proveedor::findOrFail($id_proveedor);
        return view('bd.editp',compact('proveedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_proveedor)
    {
        $proveedor=Proveedor::findOrFail($id_proveedor);
        $proveedor->id_proveedor=$request->input('id_proveedor');
        $proveedor->nombre=$request->input('nombre');
        $proveedor->apellido=$request->input('apellido');
        $proveedor->numero_celular=$request->input('numero_celular');
        $proveedor->save();
        return redirect()->route('proveedor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_proveedor)
    {
        $proveedor=Proveedor::findOrFail($id_proveedor);
        $proveedor->delete();
        return redirect()->route('proveedor.index');
    }
}
